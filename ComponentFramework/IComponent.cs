﻿using System;
using System.Collections.Generic;

namespace ComponentFramework
{
    public interface IComponent
    {
        IEnumerable<Type> ProvidedInterfaces { get; }
        IEnumerable<Type> RequiredInterfaces { get; }

        object GetInterface(Type type);
        T GetInterface<T>() where T : class;

        void InjectInterface(Type type, object impl);
        void InjectInterface<T>(object impl) where T : class;
    }
}
