﻿using System;
using System.Collections.Generic;

namespace ComponentFramework
{
    public interface IContainer
    {
        void RegisterComponent(IComponent component);
        void RegisterComponents(IEnumerable<IComponent> components);
        void RegisterComponents(params IComponent[] components);

        bool DependenciesResolved { get; }

        object GetInterface(Type type);
        T GetInterface<T>() where T : class;
    }
}
